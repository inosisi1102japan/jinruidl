![yt-dlp-web logo](static/img/logo05background.png)

# yt-dlp-web-ui

## This is a front end for [yt-dlp-web-api](https://gitlab.com/wizdevgirl1/yt-dlp-web-api). This provides an easy interface for downloading videos via yt-dlp and other functionality related to this

Requirements:

Set up yt-dlp-web-api

Clone this repository

Configure the apiurl variable in static/js/custom.js to point to your yt-dlp-web-api instance 

Install node/npm

`npm install --omit=dev`

That's all!

Once this code is hosted, add the host url to allowedorigins in conf.json in yt-dlp-web and reload the server

# License
This code is distributed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)